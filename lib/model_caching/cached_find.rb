
module ModelCaching
	module CachedFind

  		extend ActiveSupport::Concern

  		def flush_find
    		Rails.cache.delete([self.class.name, :cached_find, id])
  		end

  		included do
    		after_commit :flush_find
  		end

  		# add your static(class) methods here
  		module ClassMethods
		    def cached_find id
		      Rails.cache.fetch([self.name, :cached_find, id]) { self.find(id) }
		    end
  		end
	end
end